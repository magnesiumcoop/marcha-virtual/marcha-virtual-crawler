#FROM node:8-slim
FROM python

RUN pip install future==0.16.0
RUN pip install selenium==3.9.0
RUN pip install tqdm==4.23.4
RUN pip install pre-commit==1.16.1
RUN pip install black==19.3b0
RUN pip install pymongo
RUN pip install python-dotenv

# See https://crbug.com/795759
RUN apt-get update && apt-get install -yq libgconf-2-4

# Install latest chrome dev package and fonts to support major charsets (Chinese, Japanese, Arabic, Hebrew, Thai and a few others)
# Note: this installs the necessary libs to make the bundled version of Chromium that Puppeteer
# installs, work.
# Check available versions here: https://www.ubuntuupdates.org/package/google_chrome/stable/main/base/google-chrome-stable
ARG CHROME_VERSION="81.0.4044.113-1"
RUN wget --no-verbose -O /tmp/chrome.deb https://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_${CHROME_VERSION}_amd64.deb \
  && apt install -y /tmp/chrome.deb \
  && rm /tmp/chrome.deb
RUN apt-get update && apt-get install -y wget --no-install-recommends \
    && apt-get update \
    && apt-get install -y fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge --auto-remove -y curl \
    && rm -rf /src/*.deb

# It's a good idea to use dumb-init to help prevent zombie chrome processes.
# ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
# RUN chmod +x /usr/local/bin/dumb-init

# Install puppeteer so it's available in the container.
# RUN yarn add headless-chrome-crawler

RUN pip install pymemcache
RUN pip install requests
RUN pip install pytz
RUN pip install fake-useragent==0.1.11

ADD . /usr/src/app

WORKDIR /usr/src/app

CMD python -u /usr/src/app/ig_to_mongo.py
