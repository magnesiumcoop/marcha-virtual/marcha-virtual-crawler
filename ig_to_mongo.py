# -*- coding: utf-8 -*-
import logging
import os
import queue as Queue
import re
import threading
import time
from dataclasses import dataclass
from datetime import datetime

import pytz
import requests
from pymemcache.client import base
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

from crawler import get_posts_by_hashtag

datetime.now(tz=pytz.timezone('America/Montevideo'))

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

ig_key_pattern = r'.*instagram.com\/p\/(.*)\/'
ig_user_pattern = r'by (.*) on '
ig_date_pattern = r'((:?January|February|March|April|May|June|July|August|September|October|November|December) \d{2}, \d{4})'
ig_img_size_pattern = r'/s(\d*)x(\d*)/'
ig_hashtags_pattern = r'(^|\s)(#[a-z\d-]+)'

instagram_date_format = '%B %d, %Y'
twitter_date_format = '%a %b %d %H:%M:%S +0000 %Y'  # Wed Apr 29 23:08:19 +0000 2020


@dataclass
class Task:
    hashtag: str
    size: int
    initial_size: int
    last_try_time: float


def convert_result(ig_post_json):
    key = ig_post_json.get('key', None)
    orig_url = key
    if key:
        match = re.search(ig_key_pattern, key, re.IGNORECASE)
        if match:
            key = match.group(1)

    username = ig_post_json.get('username', None)
    alt_username = None
    caption = ig_post_json.get('caption', None)
    hashtags = []
    date = datetime.now()
    if caption:
        match_date = re.search(ig_date_pattern, caption)
        if match_date:
            date_str = match_date.group(1)
            try:
                date = datetime.strptime(date_str, instagram_date_format)
            except Exception as ignore:
                log.error('Error de fecha {}'.format(caption))
        for match in re.findall(ig_hashtags_pattern, caption):
            try:
                hashtags.append(match[1])
            except Exception as ignore:
                log.error('Error de hashtag {}'.format(caption))

        match_alt_username = re.search(ig_user_pattern, caption)
        if match_alt_username:
            alt_username = match_alt_username.group(1)
            if not username:
                username = match_alt_username.group(1)

    date_str = date.strftime(twitter_date_format)
    img_url = ig_post_json.get('img_url', None)
    img_w = 0
    img_h = 0
    if img_url:
        match_size = re.search(ig_img_size_pattern, img_url)
        if match_size:
            img_w = match_size.group(1)
            img_h = match_size.group(2)

    user_location = ig_post_json.get('location', '')
    description = ig_post_json.get('description', '')

    if key and img_url:
        # api para obtener la data del user
        profile_pic_url = "https://www.instagram.com/static/images/ico/favicon-192.png/68d99ba29cc8.png"
        try:
            URL = "https://www.instagram.com/" + username + "/?__a=1"
            r = requests.get(url=URL)
            data = r.json()
            # si responde algo tengo foto
            if data:
                profile_pic_url = data['graphql']['user']['profile_pic_url']
        except Exception as e:
            log.error('Error de foto de perfil {}'.format(str(e)))

        return orig_url, {
            "tweet_id_str": key,
            "tweet_created_at": date_str,
            "hashtags": hashtags,
            "instagram_post_url": orig_url,
            "instagram_post": True,
            "full_text": description,
            "user": {
                "id_str": username,
                "name": alt_username,
                "screen_name": username,
                "location": user_location,
                "profile_image_url": profile_pic_url,
                "profile_image_url_https": profile_pic_url,
            },
            "media": [
                {
                    "media_url": orig_url,
                    "media_url_small": img_url,
                    "media_url_thumb": orig_url + 'media/?size=t',
                    "sizes": {
                        "small": {
                            "w": img_w,
                            "h": img_h,
                            "resize": "fit"
                        }
                    }
                }
            ]
        }

    return orig_url, None


def do_the_hard_work(task):
    this_try_time = task.last_try_time
    try:
        start = time.time()
        posts = get_posts_by_hashtag(task.hashtag, task.size, True)
        for post in posts:
            post_url, converted_post = convert_result(post)
            # Lo encontre, va a la cache
            cache.set(post_url, post_url)
            if converted_post:
                # Guardo en mongo si tiene key y url
                try:
                    result = posts_col.find_one({"tweet_id_str": converted_post["tweet_id_str"]})
                    if not result:
                        posts_col.insert_one(converted_post)
                except DuplicateKeyError as ignore:
                    ignore
                except Exception as e:
                    log.error(post_url, str(e))
        end = time.time()
        this_try_time = (end - start) * 1000.0
        log.info('{} Listo, {:.3f} ms'.format(hashtag, this_try_time))

    except Exception as e:
        log.error(hashtag, str(e))

    # Devuelvo la tarea a la cola después de un tiempo
    time_sleep = int(os.getenv('THREAD_SLEEP_TIME', 100))
    log.info('Descanso {}s para {}'.format(time_sleep, task.hashtag))
    time.sleep(time_sleep)
    # Sigo
    if task.size >= 300:
        task.size = task.initial_size
    elif task.last_try_time == 0 or abs(this_try_time - task.last_try_time) < task.last_try_time:
        # Si demoré relativamente menos voy a intentar aumentar la cantidad
        task.size = task.size + 5

    task.last_try_time = this_try_time
    log.info('Encolando {}'.format(task))
    queue.put(task)


log = logging.getLogger('ig_to_mongo')
client = MongoClient(os.getenv('MONGODB_URI'))
db = client.get_database()
hashtag_col = db.get_collection(os.getenv('MONGODB_HASHTAG_COLL'))
posts_col = db.get_collection(os.getenv('MONGODB_POSTS_COLL'))
cache = base.Client(('memcached', 11211))
queue = Queue.Queue()

if __name__ == "__main__":
    log.info('start!')
    # Hashtags de la BD
    cursor = hashtag_col.find({})
    hashtags = [document['name'].replace('#', '') for document in cursor]

    # Preparo la cola de tareas
    for hashtag in hashtags:
        size = int(os.getenv('RESULT_SIZE_1', 15))
        # Al inicio size e initial_size son iguales
        task = Task(hashtag, size, size, 0)
        log.info('Encolando {}'.format(task))
        queue.put(task)

    # Loop
    while True:
        # Tomo una tarea si hay, sino espero
        task = queue.get()
        # La mando a un unevo thread
        thread = threading.Thread(target=do_the_hard_work, args=[task])
        thread.daemon = True
        thread.start()
