# Marcha Crawler
##### Basado en  [Instagram Crawler](https://github.com/huaying/instagram-crawler)

Que hace:
- Pide los posts de los usuarios según un hashtag sin utilizar la API de Insgagram. 
-- Utilizando: `crawler.py`

## Instalar
- Corre en docker, por lo que no se necesita nada extra, simplemente: 
    - Clonar el repo 
    - Crear entorno `cp .env-example .env` y configurar credenciales
    - Crear el archivo de las credenciales `cp secret.py.dist secret.py` (necesario hacerlo, pero puede quedar con los campos vacíos)
    - Correr `docker-compose up`

## Cosas que usa
1. Python
2. Chrome para docker: [headless-chrome-crawler](https://github.com/yujiosaka/headless-chrome-crawler)
2. Una version fija y estable (81) del [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/) en `./inscrawler/bin/chromedriver`
3. Selenium


### ToDo
```
- Pushear la data al mongo
```
